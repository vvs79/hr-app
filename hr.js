//UserAccounts = new Mongo.Collection('users');
PersonsList = new Mongo.Collection('persons'); // створити таблицю

Lists = new Meteor.Collection('lists');


var counter = 1;


Router.route('/', { name: 'home', template: 'home' });
Router.route('/register');
Router.route('/login');
Router.route('/addPersonForm');//,  { name: 'add_person', template: 'addPersonForm' } );
Router.route('/removePerson');
Router.route('/allResumes');

Router.configure({
    layoutTemplate: 'main'
});

Router.route('/resume/:_id', { template: 'showResume',
    data: function(){
        //console.log(this.params.someParameter);
		var currentResume = this.params._id;
		return PersonsList.findOne({ _id: currentResume });
    }
});

Router.route('/edit/:_id', { template: 'editResume',
    data: function(){
        //console.log(this.params.someParameter);
		var currentResume = this.params._id;
		return PersonsList.findOne({ _id: currentResume });
    }
});
	
	
	
if(Meteor.isClient){
	
	Template.topMenu.helpers({
	  'proff': function(){
		//var currentUserId = Meteor.userId();
        return PersonsList.find({}, { sort: {name: 1} });
      },
	  
	  'resumeCount': function(){
        return PersonsList.find({}).count();
      },
	});

	
	
	Template.mainForm.helpers({
	  'person': function(){
		//var currentUserId = Meteor.userId();
        return PersonsList.find({}, { sort: {name: 1} });
      }
	});
	

	
	
    Template.addPersonForm.events({
      'submit .firstForm': function(event){
		event.preventDefault();
        var personData = [];
		personData.push(event.target.personName.value);
		//console.log(typeof event.target.personSkill2);
		if (typeof event.target.personSkill2 !== "undefined") {
		  event.target.personSkill.value += ("," + event.target.personSkill2.value);
		  var element = document.getElementsByName("personSkill2");
          element[0].parentNode.removeChild(element[0]);
		}
		if (typeof event.target.personSkill3 !== "undefined") {
		  event.target.personSkill.value += ("," + event.target.personSkill3.value);
		  var element = document.getElementsByName("personSkill3");
          element[0].parentNode.removeChild(element[0]);
		}
		personData.push(event.target.personSkill.value);
		personData.push(event.target.personDate.value);
		personData.push(event.target.personCity.value);
		personData.push(event.target.personPhone.value);
		personData.push(event.target.personSkype.value);
		personData.push(event.target.personEmail.value);
		personData.push(event.target.personSalary.value);
		
		Meteor.call('createPerson', personData);
		
		event.target.personName.value = "";
		event.target.personSkill.value = "";
		event.target.personDate.value = "";
		event.target.personCity.value = "";
		event.target.personPhone.value = "";
		event.target.personSkype.value = "";
		event.target.personEmail.value = "";
		event.target.personSalary.value = "";
		counter = 1;
      },
	  
	  
	  'click .addSkill': function(){
        if (counter < 3)  {
		  var newspan = document.createElement('span');
          newspan.innerHTML = '<input type="text" class="form-control" name="personSkill'+(++counter)+'" placeholder="Скіли"/>';
          document.getElementById('allSkills').appendChild(newspan);
		  //document.getElementById('allSkills').innerHTML += '<input type="text" class="form-control" name="personSkill'+(++counter)+'" placeholder="Скіли"/>';
        }
	  }
    });
	
	
	
	Template.addList.events({
     'submit form': function(event){
      event.preventDefault();
      var listName = $('[name=listName]').val();
      Lists.insert({
          name: listName
      });
      $('[name=listName]').val('');
     }
    });
	
	
	
	Template.removePerson.helpers({
	  'person': function(){
		//var currentUserId = Meteor.userId();
        return PersonsList.find({}, { sort: {name: 1} });
      },
	  
	  'selectedClass': function(){
		//return this._id;
		var personId = this._id;
		var selectedPerson = Session.get('selectedPerson');
		if(personId == selectedPerson) {return "selected";}
	  },
	  
	  'selectedPerson': function(){
		var selectedPerson = Session.get('selectedPerson');
		return PersonsList.findOne({ _id: selectedPerson });
	  }
    });
	
	
	
	Template.removePerson.events({
		
	  'click .person': function(){
		var personId = this._id;
		Session.set('selectedPerson', personId);
		//var selectedPlayer = Session.get('selectedPlayer');
		//console.log(selectedPlayer);
        //console.log(this.name);
		console.log("YES " + personId);
      },
	  
      'click .remove': function(){
        var selectedPerson = Session.get('selectedPerson');
        PersonsList.remove({ _id: selectedPerson });
		//Meteor.call('removePlayer', selectedPlayer);
	  }
    });
	
	
	Template.allResumes.helpers({
	  'person': function(){
		//var currentUserId = Meteor.userId();
        return PersonsList.find({}, { sort: {name: 1} });
      }
    });
}








if(Meteor.isServer){

}




//  https://habrahabr.ru/post/208970/   first_steps

//  https://habrahabr.ru/sandbox/77384/      routing       https://github.com/iron-meteor/iron-router
//   http://meteortips.com/second-meteor-tutorial/    +++  ...first...




Meteor.methods({
   'createPerson': function(personData){
	  check(personData[0], String);
      var currentUserId = Meteor.userId();
	  PersonsList.insert({
        name: personData[0], skill: personData[1], dateOfBirth: personData[2], city: personData[3],
	    phone: personData[4], skype: personData[5], email: personData[6], salary: personData[7],
		createdBy: currentUserId, createdAt: new Date()
      });
    },
});
